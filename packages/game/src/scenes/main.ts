import { EVENTS_NAME } from '../consts';
import { Enemy } from '../classes/enemy';
import { Button } from '../classes/button';

export class MainScene extends Phaser.Scene {
  private background!: Phaser.GameObjects.Image;
  private sceneContainer!: Phaser.GameObjects.Container;
  private enemy!: Enemy;
  private attackBtn!: Button;
  private playNowBtn!: Button;
  private logo!: Button;
  private resizeHandler: () => void;

  constructor() {
    super('main-scene');

    this.resizeHandler = () => {
      this.resize();
      this.changePositions();
    };
  }

  create(): void {
    this.sceneContainer = this.add.container(this.game.scale.width / 2, 0);
    this.background = this.add.image(0, 0, 'background');

    this.initEnemy();
    this.initAttackButton();
    this.initPlayNowBtn();
    this.initLogo();
    this.sceneContainer.add([this.background, this.enemy, this.attackBtn]);

    this.resize();
    this.changePositions();

    this.initEvents();
  }

  // Initializations

  private initEnemy(): void {
    this.enemy = new Enemy({ scene: this, x: 0, y: 0 }).setScale(2);
  }

  private initAttackButton(): void {
    this.attackBtn = new Button({ scene: this, x: 0, y: this.enemy.getBounds().bottom, frame: 'btn-attack' });
    
    this.attackBtn.setInteractive().on('pointerdown', () => {
      console.log('Working!');
      this.enemy.attacksThePlayer();
    });
  }

  private initPlayNowBtn(): void {
    this.playNowBtn = new Button({ scene: this, x:  this.game.scale.width - 16, y: this.game.scale.height - 16,frame: 'btn-play-now',isPulsed: true});
    this.playNowBtn.setOrigin(1);
    this.playNowBtn.setInteractive().on('pointerdown', () => {
      document.location.href = "http://play.google.com/store/apps/";
    });
  
  }

  private initLogo(): void {
    if(this.game.scale.isGameLandscape === true){
      this.logo = new Button({ scene: this, x:16, y: 16,frame: 'logo'});
      this.logo.setOrigin(0);
    }  else if (this.game.scale.isGamePortrait === true) {
      this.logo = new Button({ scene: this, x:  this.game.scale.width/2, y:16,frame: 'logo'});
      this.logo.setOrigin(0.5,0);
    }
  }

  private initEvents(): void {
    this.game.scale.addListener(EVENTS_NAME.resize, this.resizeHandler);
  }

  // Resize

  private resize(): void {
    this.updateSceneContainerSize();
  }

  private updateSceneContainerSize() {
    const scaleX = this.game.scale.width / this.background.width;
    const scaleY = this.game.scale.height / this.background.height;
    const scale = Math.max(scaleX, scaleY);

    this.sceneContainer.setScale(scale);
  }

  private changePositions(): void {
    this.sceneContainer.setPosition(this.game.scale.width / 2, this.game.scale.height / 2);
  }
}
